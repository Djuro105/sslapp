﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SSLdemoApp.Controllers
{
    
    public class HomeController : Controller
    {
        //List<string> allowedClients = ["tjansky", "gmarkovic", "žjurić"];
        public ActionResult Index()
        {
            //this.Request.Abort();
            //var cert =  this.Request.ClientCertificate;
            var cert = GetClientCertificate();

            if (IsClientAllowed(cert))
            {
                return View();
            } else
            {
                //digni 401
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.Unauthorized);
            }
        } 
        
        // subject alternative name    DNS Name=yourdomain.com  https://stackoverflow.com/questions/6383054/add-or-create-subject-alternative-name-field-to-self-signed-certificate-using

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public HttpClientCertificate GetClientCertificate()
        {
            HttpClientCertificate clientCert;
            clientCert = this.Request.ClientCertificate;

            return clientCert;
        }

        public bool IsClientAllowed(HttpClientCertificate cert)
        {
           
            string[] allowedClients = { "tjansky", "gmarkovic", "žjurić" };

            string clientName = cert.Subject.Substring(3);  //CN=
    
            if (allowedClients.Contains(clientName))
            {
                return true;
            } else
            {
                return false;
           }
            
        }

    }
}